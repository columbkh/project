#!/usr/bin/env php
<?php

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Command\ProveVersion;
use App\Command\ProveUpdate;


$appli = new Application('appli', '1.0.0');

$pvcommand = new ProveVersion();
$prcommand = new ProveUpdate();

$appli->add($pvcommand);
$appli->add($prcommand);

$appli->run();
