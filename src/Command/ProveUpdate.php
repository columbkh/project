<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use ZipArchive;

class ProveUpdate extends Command
{
    protected function configure()
    {
        $this
            ->setName("prupt")
            ->setDescription("It proves, if the packages are updated")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = "../composer.lock";
        $path_pack = "https://packagist.org/p/";
        $pack = "";
        $text = "";

        $data = file_get_contents($path);

        $json = json_decode($data, true);

        $array1 = $json['packages'];
        $array2 = $json['packages-dev'];

        $output->writeln("\n");

        foreach ($array1 as $package) {
            $name = $package['name'];
            $version = $package['version'];
            $version = trim($version, "v");

            $data = file_get_contents($path_pack . $name . '.json');
            $json = json_decode($data, true);

            $paket = $json['packages'];
            $versii = $paket[$name];

            $dlina = count($versii);
            $keys = array_keys($versii);
            $versia = $versii[$keys[$dlina - 1]];

            $versnum = $versia['version'];
            $versnum = trim($versnum, "v");

            $text = $name;
            $status = (version_compare($version, $versnum) < 0) ? ' is not up to date. Your version is ' .
                $version . ". The newest version is " . $versnum: ' is up to date.';
            $text .= $status;
            $output->writeln($text);
            $output->writeln("\n");
        }

        $output->write("//////////////////////////////////////////////////////////////////////");
        $output->writeln("\n");

        foreach ($array2 as $package) {
            $name = $package['name'];
            $version = $package['version'];
            $version = trim($version, "v");

            $data = file_get_contents($path_pack . $name . '.json');
            $json = json_decode($data, true);

            $paket = $json['packages'];
            $keys = array_keys($paket);
            $versii = $paket[$keys[0]];

            $dlina = count($versii);
            $keys = array_keys($versii);
            $versia = $versii[$keys[$dlina - 1]];

            $versnum = $versia['version'];
            $versnum = trim($versnum, "v");

            $output->writeln("\n");

            $text = $name;
            $status = (version_compare($version, $versnum) < 0) ? ' is not up to date. Your version is ' .
                $version . ". The newest version is " . $versnum: ' is up to date.';
            $text .= $status;
            $output->writeln($text);
            $output->writeln("\n");
        }
    }
}