<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01.02.2018
 * Time: 22:27
 */

namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class GreetCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName("demo:greet")
            ->setDescription("It greets a user")
            ->addArgument('name', InputArgument::OPTIONAL, 'Who do you want to greet?')
            ->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        if($name){
            $text = "Hello, ".$name;
        }
        else {
            $text = "Hello";
        }
        if($input->getOption('yell')){
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }

}