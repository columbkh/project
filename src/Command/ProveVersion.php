<?php
/**
 * Created by PhpStorm.
 * User: Anton Khristoforov
 * Date: 02.02.2018
 * Time: 13:49
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ProveVersion extends Command
{
    protected function configure()
    {
        $this
            ->setName("prver")
            ->setDescription("It proves, if the given version is installed")
            ->addArgument('name', InputArgument::REQUIRED, 'Name of the package')
            ->addArgument('version', InputArgument::REQUIRED, 'Name of the package')
            #->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = "../composer.lock";
        $name = $input->getArgument('name');
        $version = $input->getArgument('version');
        $pack = "";
        $text = "";
        $flag = 0;

        $data = file_get_contents($path);

        $json = json_decode($data, true);

        $array1 = $json['packages'];
        $array2 = $json['packages-dev'];


        foreach ($array1 as $package) {
            if ($package['name'] === $name) {
                $pack = $package;
                $flag = 1;
                break;
            }
        }

        if ($flag === 1) {
            $version = trim($version, "v");
            $pack['version'] = trim($pack['version'], "v");
            if (version_compare($version, $pack['version']) <= 0) {
                $text = "This version is already installed. \n";
                $text .= "Current version is ". $pack['version'];
                $output->writeln($text);
            } else {
                $text = "This version is not installed. \n";
                $text .= "Current version is ". $pack['version'];
                $output->writeln($text);
            }
        } else {
            foreach ($array2 as $package) {
                if ($package['name'] === $name) {
                    $pack = $package;
                    $flag = 1;
                    break;
                }
            }
            if ($flag === 1) {
                $version = trim($version, "v");
                $pack['version'] = trim($pack['version'], "v");
                if (version_compare($version, $pack['version']) <= 0) {
                    $text = "This version is already installed. \n";
                    $text .= "Current version is ". $pack['version'];
                    $output->writeln($text);
                } else {
                    $text = "This version is not installed. \n";
                    $text .= "Current version is ". $pack["version"];
                    $output->writeln($text);
                }
            } else {
                $text = "This package is not installed \n";
                $output->writeln($text);
            }
        }
    }
}