<?php

namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class OtherCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName("demo:other")
            ->setDescription("It does some other things")
            ->addArgument('name', InputArgument::OPTIONAL, 'Who do you want to revert?')
            ->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        if($name){
            $name = strrev($name);
            $text = "Hello, ".$name;
        }
        else {
            $text = "Hello";
        }
        if($input->getOption('yell')){
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }

}