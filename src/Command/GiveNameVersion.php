<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01.02.2018
 * Time: 22:27
 */

namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class GiveNameVersion extends Command
{
    protected function configure()
    {
        $this
            ->setName("namever")
            ->setDescription("It lists the names and versions of the packages")
            ->addArgument('path', InputArgument::OPTIONAL, 'Give a path to composer.lock');
            #->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');

        $data = file_get_contents($path);

        $json = json_decode($data);

        foreach ($json->packages as $package) {
            $text =  $package->name. "-------". $package->version. "\n";
            $output->writeln($text);
        }


    }

}